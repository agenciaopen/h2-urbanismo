<?php
/**
 * Plugin Name: h2 urbanismo
 * Plugin URI: https://h2urbanismo.com.br
 * Description: post types plugin
 * Version: 1.0
 * Author: Mateus Lara
 * Author URI: https://www.mateuslara.com.br
 */

 /*
* Creating a function to create our CPT
*/
add_theme_support('post-thumbnails');

function empreendimentos_custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Empreendimentos', 'Post Type General Name', 'h2' ),
            'singular_name'       => _x( 'Empreendimento', 'Post Type Singular Name', 'h2' ),
            'menu_name'           => __( 'Empreendimentos', 'h2' ),
            'parent_item_colon'   => __( 'Parent Empreendimento', 'h2' ),
            'all_items'           => __( 'Todos os Empreendimentos', 'h2' ),
            'view_item'           => __( 'Ver Empreendimento', 'h2' ),
            'add_new_item'        => __( 'Adicionar novo Empreendimento', 'h2' ),
            'add_new'             => __( 'Adicionar novo', 'h2' ),
            'edit_item'           => __( 'Editar Empreendimento', 'h2' ),
            'update_item'         => __( 'Atualizar Empreendimento', 'h2' ),
            'search_items'        => __( 'Procurar Empreendimento', 'h2' ),
            'not_found'           => __( 'Nada encontrado', 'h2' ),
            'not_found_in_trash'  => __( 'Nada encontrada na lixeira', 'h2' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Empreendimentos', 'h2' ),
            'description'         => __( 'Cadastro de Empreendimentos e seus detalhes', 'h2' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'author', 'thumbnail', 'revisions', 'custom-fields'),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'menu_icon'           => 'dashicons-welcome-add-page',
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
      
        );
         
        // Registering your Custom Post Type
        register_post_type( 'empreendimentos', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'empreendimentos_custom_post_type', 0 );

    // Creating a Vagas Custom Post Type
function vagas_custom_post_type() {
	$labels = array(
		'name'                => __( 'Vagas' ),
		'singular_name'       => __( 'Vaga'),
		'menu_name'           => __( 'Vagas'),
		'parent_item_colon'   => __( 'Parent Vaga'),
		'all_items'           => __( 'Todas as Vagas'),
		'view_item'           => __( 'Ver Vaga'),
		'add_new_item'        => __( 'Adicionar nova Vaga'),
		'add_new'             => __( 'Adicionar nova'),
		'edit_item'           => __( 'Editar Vaga'),
		'update_item'         => __( 'Atualizar Vaga'),
		'search_items'        => __( 'Procurar Vaga'),
		'not_found'           => __( 'Nada encontrado'),
		'not_found_in_trash'  => __( 'Nada encontrado na lixeira')
	);
	$args = array(
		'label'               => __( 'Vagas'),
		'description'         => __( 'Nossas Vagas'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'author', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => false,
        'can_export'          => true,
        'menu_icon'           => 'dashicons-admin-multisite',
		'exclude_from_search' => false,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page'
);
	register_post_type( 'Vagas', $args );
}
add_action( 'init', 'vagas_custom_post_type', 0 );


    // // Creating a parceiras Custom Post Type
    // function parceiras_custom_post_type() {
    //     $labels = array(
    //         'name'                => __( 'parceiras' ),
    //         'singular_name'       => __( 'parceira'),
    //         'menu_name'           => __( 'Parceiras'),
    //         'parent_item_colon'   => __( 'Parent parceira'),
    //         'all_items'           => __( 'Todas as parceiras'),
    //         'view_item'           => __( 'Ver parceira'),
    //         'add_new_item'        => __( 'Adicionar nova parceira'),
    //         'add_new'             => __( 'Adicionar nova'),
    //         'edit_item'           => __( 'Editar parceira'),
    //         'update_item'         => __( 'Atualizar parceira'),
    //         'search_items'        => __( 'Procurar parceira'),
    //         'not_found'           => __( 'Nada encontrado'),
    //         'not_found_in_trash'  => __( 'Nada encontrado na lixeira')
    //     );
    //     $args = array(
    //         'label'               => __( 'parceiras'),
    //         'description'         => __( 'Nossas parceiras'),
    //         'labels'              => $labels,
    //         'supports'            => array( 'title', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
    //         'public'              => true,
    //         'hierarchical'        => false,
    //         'show_ui'             => true,
    //         'show_in_menu'        => true,
    //         'show_in_nav_menus'   => true,
    //         'show_in_admin_bar'   => true,
    //         'has_archive'         => false,
    //         'can_export'          => true,
    //         'menu_icon'           => 'dashicons-admin-multisite',
    //         'exclude_from_search' => false,
    //         'taxonomies' 	      => array('post_tag'),
    //         'publicly_queryable'  => true,
    //         'capability_type'     => 'page'
    // );
    //     register_post_type( 'parceiras', $args );
    // }
    // add_action( 'init', 'parceiras_custom_post_type', 0 );
    
    // Let us create Taxonomy for Custom Post Type
    add_action( 'init', 'create_emp_custom_taxonomy', 0 );
     
    //create a custom taxonomy name it "type" for your posts
    function create_emp_custom_taxonomy() {
     
      $labels = array(
        'name' => _x( 'Estado', 'taxonomy general name' ),
        'singular_name' => _x( 'Estado', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar por tipos' ),
        'all_items' => __( 'Todos os tipos' ),
        'parent_item' => __( 'Parent tipo' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item' => __( 'Editar tipo' ), 
        'update_item' => __( 'Atualizar tipo' ),
        'add_new_item' => __( 'Adicionar novo Estado' ),
        'new_item_name' => __( 'Novo Estado' ),
        'menu_name' => __( 'Estados' ),
      ); 	
     
      register_taxonomy('estado',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'estado' ),
      ));
    }
    
    // Let us create Taxonomy for Custom Post Type
    add_action( 'init', 'create_emp_custom_taxonomy_service', 0 );
     
    //create a custom taxonomy name it "type" for your posts
    function create_emp_custom_taxonomy_service() {
     
      $labels = array(
        'name' => _x( 'Cidade', 'taxonomy general name' ),
        'singular_name' => _x( 'Cidade', 'taxonomy singular name' ),
        'search_items' =>  __( 'Procurar por tipos' ),
        'all_items' => __( 'Todos os tipos' ),
        'parent_item' => __( 'Parent tipo' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item' => __( 'Editar tipo' ), 
        'update_item' => __( 'Atualizar tipo' ),
        'add_new_item' => __( 'Adicionar novo Cidade' ),
        'new_item_name' => __( 'Novo Cidade' ),
        'menu_name' => __( 'Cidades' ),
      ); 	
     
      register_taxonomy('cidades',array('empreendimentos'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'cidades' ),
      ));
    }

    add_action( 'init', 'create_emp_custom_taxonomy_parceiros', 0 );

        //create a custom taxonomy name it "type" for your posts
        function create_emp_custom_taxonomy_parceiros() {
     
          $labels = array(
            'name' => _x( 'Parceiro', 'taxonomy general name' ),
            'singular_name' => _x( 'Parceiro', 'taxonomy singular name' ),
            'search_items' =>  __( 'Procurar por tipos' ),
            'all_items' => __( 'Todos os tipos' ),
            'parent_item' => __( 'Parent tipo' ),
            'parent_item_colon' => __( 'Parent Type:' ),
            'edit_item' => __( 'Editar tipo' ), 
            'update_item' => __( 'Atualizar tipo' ),
            'add_new_item' => __( 'Adicionar novo Parceiro' ),
            'new_item_name' => __( 'Novo Parceiro' ),
            'menu_name' => __( 'Parceiros' ),
          ); 	
         
          register_taxonomy('parceiros',array('empreendimentos'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'rewrite' => array( 'slug' => 'parceiros' ),
          ));
        }