<?php
/**
*
* Template Name: Parceiros
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section class="content meet" id="main">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="col-md-12">
                <?php echo do_shortcode('[searchandfilter id="159"]'); ?>
            </div>
            <div class="col-md-12" id="">
                <?php echo do_shortcode('[searchandfilter id="159" show="results"]');?>
            </div>
        </div>
    </div>
</section><!--/.content-->


<section class="newsletter" id="parceira"> 
    <div class="container h-100">
        <div class="row justify-content-center align-items-stretch">
            <div class="col-md-8 text-center">
                <h2>Seja nosso parceiro também</h2>
                <h3>Cadastre sua empresa e junte-se a nós na construção de novos empreendimentos únicos. Um investimento com excelentes retornos para todos os envolvidos. </h3>
                <?php echo do_shortcode('[contact-form-7 id="170" title="Formulário de empresa parceira"]');?>
            </div>            
        </div><!--/.container-->
    </div><!--/.row-->
</section><!--/.newsletter-->

<?php get_footer(); ?>

             