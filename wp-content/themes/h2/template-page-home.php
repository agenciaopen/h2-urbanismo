<?php
/**
*
* Template Name: Home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
 
// first, get the image object returned by ACF
 $image_object = get_field('imagem_principal');

if(wp_is_mobile()):
   
    // and the image size you want to return
    $image_size = '2048x2048';

    // now, we'll exctract the image URL from $image_object
    $image_url = $image_object['sizes'][$image_size];
else:
   // and the image size you want to return
   $image_size = 'large';

   // now, we'll exctract the image URL from $image_object
   $image_url = $image_object['sizes'][$image_size];
endif;

?>



<section class="main" style="background-image: url('<?php echo $image_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-8 text-center">
                <h1>
                    <?php the_field( 'titulo_principal', $page_ID, false, false  ); ?>
                </h1>
                <h2><?php the_field( 'descricao_principal', $page_ID, false, false  ); ?></h2>
                <?php $botao_principal = get_field( 'botao_principal' ); ?>
                <?php if ( $botao_principal ) : ?>
                    <a href="<?php echo esc_url( $botao_principal['url'] ); ?>" target="<?php echo esc_attr( $botao_principal['target'] ); ?>">
                        <div class="btn btn_third">
                            <?php echo esc_html( $botao_principal['title'] ); ?>
                        </div>
                    </a>
                <?php endif; ?>
               
            </div>
        </div>
    </div>
</section><!-- /.main -->

    <section class="content meet" id="lotes-a-venda">
                    <div class="container h-100"  id="main">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-10 col-lg-10 text-center">
                                <h2> <?php the_field( 'titulo_empreendimentos', $page_ID ); ?></h2>
                                <p> <?php the_field( 'subtitulo_empreendimentos', false, false, $page_ID ); ?></p>
                            </div>
                            <div class="col-md-12" >
                                <?php echo do_shortcode('[searchandfilter id="171"]');?>
                            </div>
                            <div class="col-md-12 ">
                                <?php echo do_shortcode('[searchandfilter id="171" show="results"]');?>
                            </div>
                            
           
                        </div>
                       
                    </div>
                </section><!-- /.content -->
<?php 
// first, get the image object returned by ACF
$image_object = get_field('imagem_sobre');

if(wp_is_mobile()):
   
    // and the image size you want to return
    $image_size = '2048x2048';

    // now, we'll exctract the image URL from $image_object
    $image_url = $image_object['sizes'][$image_size];
else:
   // and the image size you want to return
   $image_size = 'large';

   // now, we'll exctract the image URL from $image_object
   $image_url = $image_object['sizes'][$image_size];
endif;

?>



<section class="about" style="background-image: url('<?php echo $image_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12 text-center">
                <h2>
                    <?php the_field( 'titulo_sobre', $page_ID, false, false); ?>
                </h2>
                <h3><?php the_field( 'subtitulo_sobre', $page_ID, false, false); ?></h3>
                
                <?php $botao_sobre = get_field( 'botao_sobre' ); ?>
                <?php if ( $botao_sobre ) : ?>
                    <a href="<?php echo esc_url( $botao_sobre['url'] ); ?>" target="<?php echo esc_attr( $botao_sobre['target'] ); ?>">
                        <div class="btn btn_secondary">
                            <?php echo esc_html( $botao_sobre['title'] ); ?>
                        </div>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.about -->


<?php //get_template_part( 'templates/global/template-part', 'testimonials' ); ?>

<?php get_template_part( 'templates/global/template-part', 'newsletter' ); ?>


<?php get_footer(); ?>