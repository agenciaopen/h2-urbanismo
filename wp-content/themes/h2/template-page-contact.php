<?php
/**
*
* Template Name: Contato
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>


<section id="content" class="contact pt-md-0">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
           <div class="col-md-10 text-center">
                <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?php the_field( 'titulo_fale_conosco' ); ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><?php the_field( 'titulo_trabalhe_conosco' ); ?></a>
                    </li>
                </ul>
           </div>
           <div class="col-md-8 col-lg-10 text-center">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <p><?php the_field( 'descricao_fale_conosco' ); ?></p>
                        <?php the_field( 'formulario_fale_conosco' ); ?>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <p><?php the_field( 'descricao_trabalhe_conosco' ); ?></p>
                        <?php the_field( 'formulario_trabalhe_conosco' ); ?>
                    </div>
                </div>
           </div>
        </div>
    </div>
</section><!--/.content-->



<?php get_template_part( 'templates/global/template-part', 'newsletter' ); ?>

<?php get_footer(); ?>