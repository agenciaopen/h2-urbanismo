       var header = $("#nav_main");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                header.addClass("scrolled");
            } else {
                header.removeClass("scrolled");
            }
        });
        $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
            $(".navbar-collapse").addClass("show");
        })
          
        $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function () {
            $(".navbar-collapse").removeClass("show");
        })
       
        /*==============================================================
        navbar fixed top
        ==============================================================*/
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 100;
        var navbarHeight = $('#nav_main').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }

// var behavior = function (val) {
//     return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
// },
//     options = {
//         onKeyPress: function (val, e, field, options) {
//             field.mask(behavior.apply({}, arguments), options);
//         }
//     };

// $('input.tel, input.wpcf7-tel').mask(behavior, options);

$('.carousel_testimonials').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    cssEase: 'linear',
    adaptiveHeight: true
});
$('.carousel_empreendimentos').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    arrows: false,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    cssEase: 'linear',
    adaptiveHeight: true
});
// $('.carousel_featured').slick({
//     dots: false,
//     slidesPerRow: 3,
//     rows: 4,
//     responsive: [
//         {
//             breakpoint: 767,
//             settings: {
//                 slidesPerRow: 1,
//                 rows: 1,
//             }
//         }
//     ]
// });

/*
accordion
*/
$('#accordion .collapse').on('shown.bs.collapse', function (e) {
    $(this).prev().addClass('active');
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');
    var additionalOffset = 60;
    if($card.prevAll().filter($open.closest('.card')).length !== 0)
    {
          additionalOffset =  $open.height();
    }
    $('html,body').animate({
      scrollTop: $card.offset().top - additionalOffset
    }, 500);
});
$('#accordion .collapse').on('hidden.bs.collapse', function () {
    $(this).prev().removeClass('active');
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});

var activeTab = localStorage.getItem('activeTab');
if(activeTab){
    $('.nav-tabs a[href="' + activeTab + '"]').tab('show');
}

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
	    adaptiveHeight: true,
    asNavFor: '.slider-nav'
});
$('.slider-for').slickLightbox({
    itemSelector        : 'a',
    navigateByKeyboard  : true
  });
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical:true,
    asNavFor: '.slider-for',
    dots: false,
    focusOnSelect: true,
    verticalSwiping:true,
    responsive: [
    {
        breakpoint: 992,
        settings: {
          vertical: false,
        }
    },
    {
      breakpoint: 768,
      settings: {
        vertical: false,
      }
    },
    {
      breakpoint: 580,
      settings: {
        vertical: false,
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 380,
      settings: {
        vertical: false,
        slidesToShow: 2,
      }
    }
    ]
});