<?php
/**
*
* Template Name: Institucional
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>





    <section class="content vizinhos">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-10 col-lg-10 text-center ">
                                <h2> <?php the_field( 'titulo_-_somos_seus_vizinhos', $page_ID ); ?></h2>
                            </div>
                            <div class="col-md-10 text-center">
                                <p><?php the_field( 'descricao_-_somos_seus_vizinhos' ); ?></p>
                            </div>
                            <div class="col-md-4 mt-4 text-center text-lg-right">
                                <?php $quero_ser_parceiro_somos_seus_vizinhos = get_field( 'quero_ser_parceiro_somos_seus_vizinhos' ); ?>
                                <?php if ( $quero_ser_parceiro_somos_seus_vizinhos ) : ?>
                                    <a href="<?php echo esc_url( $quero_ser_parceiro_somos_seus_vizinhos['url'] ); ?>" target="<?php echo esc_attr( $quero_ser_parceiro_somos_seus_vizinhos['target'] ); ?>">
                                        <div class="btn btn_secondary_inverse">
                                            <?php echo esc_html( $quero_ser_parceiro_somos_seus_vizinhos['title'] ); ?>

                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4 mt-4 text-center text-lg-left">
                                <?php $conferir_lotes_a_venda_somos_seus_vizinhos = get_field( 'conferir_lotes_a_venda_somos_seus_vizinhos' ); ?>
                                <?php if ( $conferir_lotes_a_venda_somos_seus_vizinhos ) : ?>
                                    <a href="<?php echo esc_url( $conferir_lotes_a_venda_somos_seus_vizinhos['url'] ); ?>" target="<?php echo esc_attr( $conferir_lotes_a_venda_somos_seus_vizinhos['target'] ); ?>">
                                        <div class="btn btn_secondary_inverse">
                                            <?php echo esc_html( $conferir_lotes_a_venda_somos_seus_vizinhos['title'] ); ?>

                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                       
                    </div>
                </section><!-- /.content -->




<section class="pilares">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-center">
            <div class="col-md-12 text-center mb-5">
                <h2>
                    <?php the_field( 'titulo_nossos_pilares', $page_ID, false, false); ?>
                </h2>
               
            </div>
            <?php if ( have_rows( 'cadastro_de_pilares',  $page_ID ) ) : ?>
                <?php while ( have_rows( 'cadastro_de_pilares',  $page_ID ) ) : the_row(); ?>
                    <div class="col-md-4 text-center item">
                        <?php if ( get_sub_field( 'icone' ) ) : ?>
                            <img src="<?php the_sub_field( 'icone' ); ?>" alt="<?php the_sub_field( 'alt_icone' ); ?>" class="img-fluid" />
                        <?php endif ?>
                        <h3><?php the_sub_field( 'titulo' ); ?></h3>
                        <p><?php the_sub_field( 'descricao', false, false ); ?></p>
                    </div>
                   
                <?php endwhile; ?>
            <?php else : ?>
                <?php // no rows found ?>
            <?php endif; ?>
        </div>
    </div>
</section><!-- /.about -->


<?php //get_template_part( 'templates/global/template-part', 'testimonials' ); ?>

<?php get_template_part( 'templates/global/template-part', 'newsletter' ); ?>


<?php get_footer(); ?>