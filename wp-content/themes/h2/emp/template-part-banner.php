<?php
global $post;
$page_ID = $post->ID;
// get page ID

if(wp_is_mobile()):
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
else:
    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
endif;
?>
<?php if( get_field('titulo_principal', $page_ID) ): ?>
    <?php $title = get_field('titulo_principal', $page_ID); ?>
<?php else: ?>
    <?php $title = get_the_title(); ?>
<?php endif; ?>

<section class="main inner" style="background-image:url('<?php echo $featured_img_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-end justify-content-center">
            <div class="col-md-12 col-lg-9 text-center">
                <h1 class="text-green"><?php the_field( 'titulo_banner_empreendimento' ); ?></h1>
                <p class="text-white banner"><?php the_field('resumo', $page_ID); ?></p>
            </div>
        </div>
    </div>
</section><!-- /.main -->