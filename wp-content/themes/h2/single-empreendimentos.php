<?php
/**
*
* single page for cpt planos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'emp/template-part', 'banner' ); ?>

    <section class="empreendimento">
        <div class="container h-100">
            <div class="row h-100 align-items-start justify-content-center">
                <div class="col-md-8 text-center">
                    <h2><?php the_field( 'titulo_falando_um_beneficio_do_empreendimento_ou_localidade_' ); ?></h2>
                    <p><?php the_field( 'descricao_falando_um_beneficio_do_empreendimento_ou_localidade' ); ?></p>
                </div>
                <?php $cadastro_de_imagens_images = get_field( 'cadastro_de_imagens' ); ?>
                <?php if ( $cadastro_de_imagens_images ) :  ?>
                    <div class="col-md-9 slider-for">
                        <?php foreach ( $cadastro_de_imagens_images as $cadastro_de_imagens_image ): ?>
                            <div class="thumb">
                                <a href="<?php echo esc_url( $cadastro_de_imagens_image['sizes']['large'] ); ?>">
                                    <img src="<?php echo esc_url( $cadastro_de_imagens_image['sizes']['large'] ); ?>" alt="<?php echo esc_attr( $cadastro_de_imagens_image['alt'] ); ?>" class="img-fluid" />
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <?php if ( have_rows( 'cadastro_de_videos' ) ) : ?>
                            <?php while ( have_rows( 'cadastro_de_videos' ) ) : the_row(); ?>
                                <div class="thumb">
                                    <?php 
                                        $url =  get_sub_field('video');
                                        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                                        $youtube_id = $match[1];
                                        // $base_url = 'https://www.youtube.com/watch?v=';
                                        // $url_final = $base + $youtube_id;
                                    ?>
                                    <a href="http://www.youtube.com/embed/<?php echo $youtube_id; ?>">
                                        <img src="http://i.ytimg.com/vi/<?php echo $youtube_id;?>/maxresdefault.jpg" alt="<?php the_sub_field( 'alt_thumb_video' ); ?>" class="img-fluid" />
                                    </a>
                                </div>
                                
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                    </div>
                    <div class="slider slider-nav thumb-image col-md-3">
                        <?php foreach ( $cadastro_de_imagens_images as $cadastro_de_imagens_image ): ?>
                            <div class="thumbnail-image">
                                <div class="thumbImg">
                                    <img src="<?php echo esc_url( $cadastro_de_imagens_image['sizes']['thumbnail'] ); ?>" alt="<?php echo esc_attr( $cadastro_de_imagens_image['alt'] ); ?>" />
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php if ( have_rows( 'cadastro_de_videos' ) ) : ?>
                            <?php while ( have_rows( 'cadastro_de_videos' ) ) : the_row(); ?>
                                <div class="thumbnail-image">
                                    <div class="thumbImg">
                                        <?php 
                                            $url =  get_sub_field('video');
                                            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                                            $youtube_id = $match[1];
                                        ?>
                                            <img src="http://i.ytimg.com/vi/<?php echo $youtube_id;?>/maxresdefault.jpg" alt="" class="img-fluid" style="max-height: 150px; width: 100%;" />
                                        </a>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="col-md-12 mt-4 text-center">
                    <?php $botao_comprar_lote_ = get_field( 'botao_comprar_lote_' ); ?>
                    <?php //if ( $botao_comprar_lote_ ) : ?>
                        <div data-toggle="modal" data-target="#exampleModalCenterEmp">
                            <div class="btn btn_first text-uppercase">
								 quero comprar esse lote
                                <?php //echo esc_html( $botao_comprar_lote_['title'] ); ?>
                            </div>
                        </div>
                    <?php //endif; ?>
                </div>
            </div>
        </div>
        <div class="container details mt-4 mb-4">
            <div class="row h-100 align-items-center justify-content-start">
                <div class="col-md-12 text-center mb-4">
                    <h2>Detalhes do empreendimento</h2>
                </div>
                <?php if ( have_rows( 'cadastro_de_detalhes_do_empreendimento' ) ) : ?>
                    <?php while ( have_rows( 'cadastro_de_detalhes_do_empreendimento' ) ) : the_row(); ?>
                        <div class="col-md-4 text-center">
                            <?php if ( get_sub_field( 'icone' ) ) : ?>
                                <img src="<?php the_sub_field( 'icone' ); ?>" class="img-fluid" alt="<?php the_sub_field( 'titulo' ); ?>" title="<?php the_sub_field( 'titulo' ); ?>" />
                            <?php endif ?><br>
                            <h3 class="mt-4"><?php the_sub_field( 'titulo' ); ?></h3>

                            <p><?php the_sub_field( 'descricao', false,false ); ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="map">
        <div class="container-fluid p-0">
            <div data-toggle="modal" data-target="#exampleModalCenter">
                <?php $imagem_do_mapa = get_field( 'imagem_do_mapa' ); ?>
                <img src="<?php echo esc_url( $imagem_do_mapa['url'] ); ?>" class="img-fluid" alt="" title="" loading="lazy">
            </div>
        </div>
    </section>
    <div class="modal fade modal_map" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">fechar &times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <?php the_field( 'link_para_o_mapa' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="aboutemp">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-md-10">
                    <p>
                        <?php the_field( 'texto_sobre_o_empreendimento' ); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="statusemp">
        <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-center">
                <div class="col-md-12 text-center">
                    <h2>
                        <?php the_field( 'titulo_status_da_obra' ); ?>
                    </h2>
                    <p>
                        <?php the_field( 'descricao_status_da_obra' ); ?>
                    </p>
                </div>
                <?php if ( have_rows( 'cadastro_de_itens_status_da_obra' ) ) : ?>
                    <?php while ( have_rows( 'cadastro_de_itens_status_da_obra' ) ) : the_row(); ?>
				                            <div class="col-md-2">
<div class="progress" data-percentage="<?php the_sub_field( 'terraplanagem' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'terraplanagem' ); ?>%<br>
					</div>
				</div>
			</div>
							<h3>Terraplanagem</h3>
                            </div>
				                            <div class="col-md-2">
												<div class="progress" data-percentage="<?php the_sub_field( 'pavimentacao' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'pavimentacao' ); ?>%<br>
					</div>
				</div>
			</div>

												<h3>Pavimentação</h3>
                            </div>
				                            <div class="col-md-2">
												<div class="progress" data-percentage="<?php the_sub_field( 'drenagem' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'pavimentacao' ); ?>%<br>
					</div>
				</div>
			</div>
												<h3>Drenagem</h3>
                            </div>
				                            <div class="col-md-2">
												<div class="progress" data-percentage="<?php the_sub_field( 'sistema_de_agua_e_esgoto' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'pavimentacao' ); ?>%<br>
					</div>
				</div>
			</div>
												<h3>Sistema de água e esgoto</h3>
                            </div>
				                            <div class="col-md-2">
												<div class="progress" data-percentage="<?php the_sub_field( 'rede_eletrica' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'pavimentacao' ); ?>%<br>
					</div>
				</div>
			</div>
												<h3>Rede Elétrica</h3>
                            </div>
				                            <div class="col-md-2">
												<div class="progress" data-percentage="<?php the_sub_field( 'sistema_de_lazer' ); ?>">
				<span class="progress-left">
					<span class="progress-bar"></span>
				</span>
				<span class="progress-right">
					<span class="progress-bar"></span>
				</span>
				<div class="progress-value">
					<div>
						<?php the_sub_field( 'pavimentacao' ); ?>%<br>
					</div>
				</div>
			</div>
												<h3>Sistema de lazer</h3>
                            </div>
								

                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    

<div class="modal fade modal_map" id="exampleModalCenterEmp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg p-0" role="document">
            <div class="modal-content">
                <div class="modal-header d-none">
                    
                </div>
                <div class="modal-body p-0">
                <section class="newsletter" id=""> 
                        <div class="container h-100">
                            <div class="row justify-content-center align-items-stretch">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                                <div class="col-md-8 text-center">
                                    <h2>Excelente escolha!  <br>
                    <h3><b>Vamos dar mais um passo na <br>
                    construção desse sonho?</b> </small></h3>
                                    <p class="text-white my-3">Cadastre-se para que nossos corretores entrem em contato com você. Juntos encontraremos as melhores condições para você conquistar esse sonho.</p>
                                    <?php echo do_shortcode('[contact-form-7 id="163" title="Empreendimento"]'); ?>
                                </div>            
                            </div><!--/.container-->
                        </div><!--/.row-->
                    </section><!--/.newsletter-->
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>

<?php get_template_part( 'templates/global/template-part', 'newsletter' ); ?>

<?php get_footer(); ?>
