<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Configurações gerais',
		'menu_title'	=> 'Configurações gerais',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}
// admin config acf

add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
// featured image pages

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/inc/navbar/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

function prefix_modify_nav_menu_args( $args ) {
    return array_merge( $args, array(
        'walker' => new WP_Bootstrap_Navwalker(),
    ) );
}
add_filter( 'wp_nav_menu_args', 'prefix_modify_nav_menu_args' );

/**
 * Register nav menu primary
 */
register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'h2' ),
) );

/**
 * enqueue global styles
 */
function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'global-h2', get_template_directory_uri() . '/css/style.min.css', array(), rand(111,9999), 'all'  );
	
	if ( have_rows( 'cadastro_de_redes_sociais', 'option' ) ) :
		wp_enqueue_style( 'fa-h2', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', array(), rand(111,9999), 'all'  );
	endif;

	wp_deregister_script('jquery');
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'bs4-h2', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),  '', 'all' );
	
	if (is_singular('empreendimentos') || (is_page('template-page-contact.php'))):
		wp_enqueue_script('amask', '//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js?ver=5.4.2', array('jquery'));
		wp_enqueue_script( 'MASK-h2-js', get_template_directory_uri() . '/js/mask.js', array('jquery'),  rand(111,9999), 'all' );

	endif;
	
	if (is_singular('empreendimentos')):
		wp_enqueue_script( 'slicks-h2-js',  get_template_directory_uri() . '/js/slick-lightbox.min.js', array('jquery'),  rand(111,9999), 'all' );
		wp_enqueue_style( 'slickts-h2', get_template_directory_uri() . '/css/slick-lightbox.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_script('slick-h2-js', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js', array('jquery') , rand(111,9999), 'all' );
		wp_enqueue_style( 'slick-h2', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css', array(), rand(111,9999), 'all'  );
		wp_enqueue_style( 'slickt-h2', 'https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css', array(), rand(111,9999), 'all'  );
	endif;
	
	wp_enqueue_script( 'global-h2-js', get_template_directory_uri() . '/js/global.js', array('jquery'),  rand(111,9999), 'all' );

}


class FLHM_HTML_Compression
{
protected $flhm_compress_css = true;
protected $flhm_compress_js = true;
protected $flhm_info_comment = true;
protected $flhm_remove_comments = true;
protected $html;
public function __construct($html)
{
if (!empty($html))
{
$this->flhm_parseHTML($html);
}
}
public function __toString()
{
return $this->html;
}
protected function flhm_bottomComment($raw, $compressed)
{
$raw = strlen($raw);
$compressed = strlen($compressed);
$savings = ($raw-$compressed) / $raw * 100;
$savings = round($savings, 2);
return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
}
protected function flhm_minifyHTML($html)
{
$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
$overriding = false;
$raw_tag = false;
$html = '';
foreach ($matches as $token)
{
$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
$content = $token[0];
if (is_null($tag))
{
if ( !empty($token['script']) )
{
$strip = $this->flhm_compress_js;
}
else if ( !empty($token['style']) )
{
$strip = $this->flhm_compress_css;
}
else if ($content == '<!--wp-html-compression no compression-->')
{
$overriding = !$overriding; 
continue;
}
else if ($this->flhm_remove_comments)
{
if (!$overriding && $raw_tag != 'textarea')
{
$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
}
}
}
else
{
if ($tag == 'pre' || $tag == 'textarea')
{
$raw_tag = $tag;
}
else if ($tag == '/pre' || $tag == '/textarea')
{
$raw_tag = false;
}
else
{
if ($raw_tag || $overriding)
{
$strip = false;
}
else
{
$strip = true; 
$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
$content = str_replace(' />', '/>', $content);
}
}
} 
if ($strip)
{
$content = $this->flhm_removeWhiteSpace($content);
}
$html .= $content;
} 
return $html;
} 
public function flhm_parseHTML($html)
{
$this->html = $this->flhm_minifyHTML($html);
if ($this->flhm_info_comment)
{
$this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
}
}
protected function flhm_removeWhiteSpace($str)
{
$str = str_replace("\t", ' ', $str);
$str = str_replace("\n",  '', $str);
$str = str_replace("\r",  '', $str);
while (stristr($str, '  '))
{
$str = str_replace('  ', ' ', $str);
}   
return $str;
}
}
function flhm_wp_html_compression_finish($html)
{
return new FLHM_HTML_Compression($html);
}
function flhm_wp_html_compression_start()
{
ob_start('flhm_wp_html_compression_finish');
}
add_action('get_header', 'flhm_wp_html_compression_start');


function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    if ( strpos( $url, 'jquery.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}
add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );


function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}    
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );


add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

    $args = array(  
        'post_type' => 'empreendimentos',
        'post_status' => 'publish',
        'posts_per_page' => -1, 
    );

    $loop = new WP_Query( $args ); 
        
    while ( $loop->have_posts() ) : $loop->the_post(); 
       // Save a basic text value.
	$field_key = "field_5f77971d9673c";
	$post_id = get_the_ID();
	$value = get_the_title($post_id);

	update_field( $field_key, $value, $post_id );
    endwhile;

    wp_reset_postdata(); 


 /**
 * Remove wp tags
 */
 require_once('inc/remove-wp.php');

/**
* Custom login page
*/
require_once('inc/login-page.php');

/**
* Custom admin page
*/
require_once('inc/wp-admin.php');


/**
* Upload SVG files
*/
require_once('inc/svg-upload.php');

/**
* Remove emojis
*/
require_once('inc/remove-emojis.php');


remove_filter( 'the_content', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

remove_filter ('acf_the_content', 'wpautop');

add_filter('wpcf7_autop_or_not', '__return_false');

//   class FLHM_HTML_Compression
// {
// protected $flhm_compress_css = true;
// protected $flhm_compress_js = true;
// protected $flhm_info_comment = true;
// protected $flhm_remove_comments = true;
// protected $html;
// public function __construct($html)
// {
// if (!empty($html))
// {
// $this->flhm_parseHTML($html);
// }
// }
// public function __toString()
// {
// return $this->html;
// }
// protected function flhm_bottomComment($raw, $compressed)
// {
// $raw = strlen($raw);
// $compressed = strlen($compressed);
// $savings = ($raw-$compressed) / $raw * 100;
// $savings = round($savings, 2);
// return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
// }
// protected function flhm_minifyHTML($html)
// {
// $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
// preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
// $overriding = false;
// $raw_tag = false;
// $html = '';
// foreach ($matches as $token)
// {
// $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
// $content = $token[0];
// if (is_null($tag))
// {
// if ( !empty($token['script']) )
// {
// $strip = $this->flhm_compress_js;
// }
// else if ( !empty($token['style']) )
// {
// $strip = $this->flhm_compress_css;
// }
// else if ($content == '<!--wp-html-compression no compression-->')
// {
// $overriding = !$overriding; 
// continue;
// }
// else if ($this->flhm_remove_comments)
// {
// if (!$overriding && $raw_tag != 'textarea')
// {
// $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
// }
// }
// }
// else
// {
// if ($tag == 'pre' || $tag == 'textarea')
// {
// $raw_tag = $tag;
// }
// else if ($tag == '/pre' || $tag == '/textarea')
// {
// $raw_tag = false;
// }
// else
// {
// if ($raw_tag || $overriding)
// {
// $strip = false;
// }
// else
// {
// $strip = true; 
// $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content); 
// $content = str_replace(' />', '/>', $content);
// }
// }
// } 
// if ($strip)
// {
// $content = $this->flhm_removeWhiteSpace($content);
// }
// $html .= $content;
// } 
// return $html;
// } 
// public function flhm_parseHTML($html)
// {
// $this->html = $this->flhm_minifyHTML($html);
// if ($this->flhm_info_comment)
// {
// $this->html .= "\n" . $this->flhm_bottomComment($html, $this->html);
// }
// }
// protected function flhm_removeWhiteSpace($str)
// {
// $str = str_replace("\t", ' ', $str);
// $str = str_replace("\n",  '', $str);
// $str = str_replace("\r",  '', $str);
// while (stristr($str, '  '))
// {
// $str = str_replace('  ', ' ', $str);
// }   
// return $str;
// }
// }
// function flhm_wp_html_compression_finish($html)
// {
// return new FLHM_HTML_Compression($html);
// }
// function flhm_wp_html_compression_start()
// {
// ob_start('flhm_wp_html_compression_finish');
// }
// add_action('get_header', 'flhm_wp_html_compression_start');

// function flexible_content_to_post_content( $post_id ) {

// 	$post_type = 'post';

// 	//Check if we are saving a books post type
// 	if( get_post_type( $post_id ) != $post_type)
// 		return;

// 	//Check it's not an auto save routine
// 	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
// 		return;

// 	//The Post Content
// 	$post_content = '';

// // check if the repeater field has rows of data
// if( have_rows('cadastro_de_secoes') ):

//  	// loop through the rows of data
//     while ( have_rows('cadastro_de_secoes') ) : the_row();

//        //Loop the flexible content rows
// 	if( have_rows('cadastro_de_conteudo') ):
// 		while ( have_rows('cadastro_de_conteudo') ) : the_row();
		
// 			//TEXT BLOCK
// 			if( get_row_layout() == 'texto' ):
				
// 				$post_content .= get_sub_field('texto');
	
// 			endif;
				
// 		endwhile;
// 	endif;

//     endwhile;

// else :

//     // no rows found

// endif;


	
	
	
//     //If calling wp_update_post, unhook this function so it doesn't loop infinitely
//     remove_action('save_post', 'flexible_content_to_post_content');

// 	// call wp_update_post update, which calls save_post again. E.g:
//     wp_update_post(array('ID' => $post_id, 'post_content' => $post_content));

//     // re-hook this function
//     add_action('save_post', 'flexible_content_to_post_content');
	
// }
// add_action('save_post', 'flexible_content_to_post_content_case');

// function flexible_content_to_post_content_case( $post_id ) {

// 	$post_type = 'case';

// 	//Check if we are saving a books post type
// 	if( get_post_type( $post_id ) != $post_type)
// 		return;

// 	//Check it's not an auto save routine
// 	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
// 		return;

// 	//The Post Content
// 	$post_content = '';


				
// 				$post_content .= get_field('conteudo_solucao');
	



	
	
	
//     //If calling wp_update_post, unhook this function so it doesn't loop infinitely
//     remove_action('save_post', 'flexible_content_to_post_content_case');

// 	// call wp_update_post update, which calls save_post again. E.g:
//     wp_update_post(array('ID' => $post_id, 'post_content' => $post_content));

//     // re-hook this function
//     add_action('save_post', 'flexible_content_to_post_content_case');
	
// }
// add_action('save_post', 'flexible_content_to_post_content_case');




?>
