<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	<div class='search-filter-results-list carousel_empreendimentoss row m-0'>
	<?php
		while ($query->have_posts())
		{
			$query->the_post();
			
			?>
			<div class="item text-center col-md-4">
                                            <?php
                                            $featured_img_url = get_the_post_thumbnail_url(get_the_ID($post),'full'); 
                                            ?>

                                            <div class="col-md-12 bg" style="background-image: url('<?php echo $featured_img_url; ?>')">
                                            </div>
                                            <div class="card-body">
                                                <h2><?php echo the_title(); ?></h2>
                                                <p><?php the_field('resumo', $post);?></p>
                                            </div>
                                            <div class="card-footer">
                                                <a href="<?php echo get_permalink() ?>">
                                                    Saiba mais
                                                </a>
                                            </div>
                                        </div>
			
			<?php
		}
	?>
    </div>
<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>