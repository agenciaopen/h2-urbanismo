<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	<div class='search-filter-results-list carousel_empreendimentoss row m-0'>
	<div class="col-md-12 text-center" id="title">
				
			</div>
	<?php
		while ($query->have_posts())
		{
			$query->the_post();
			
			?>
			
			
			<?php
                        $terms = get_the_terms($post->ID, 'parceiros');
                            if( $terms):
                                foreach( $terms as $t ):
                        ?>
                        
									<?php
									$taxonomy_prefix = 'parceiros';
									$term_id = $t->term_id ;
									$term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

									?>
									<div class="item text-center col-md-4 parceiro_<?php echo $t->term_id;?>">
										<?php echo "<style>" ?>
											.parceiro_<?php echo $t->term_id;?> ~ .parceiro_<?php echo $t->term_id;?>{
												display: none;
											}
										<?php echo "</style>" ?>
										<?php if ( get_field( 'logo', $term_id_prefixed ) ) : ?>
											<img src="<?php the_field( 'logo', $term_id_prefixed  ); ?>" class="d-none"/>
										<?php endif ?>
										<h5 class="">
											<?php echo $t->name;?>
										</h5>
									</div>

                        <?php
                                endforeach;
                            endif;
                        ?>


			
			<?php
		}
	?>
    </div>
<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>
<script>
	
	var title = $("select[name='_sfm_titulo_empreendimento[]'] option:not(.sf-item-0):selected" ).text();
	console.log(title);
	if (title ==''){

	}else{
		$('#title').html('<h2>Empresas parceiras no projeto do <br>' + title + '</h2> <p>Aqui estão listadas todas as empresas que participaram do projeto do <b>' + title + '</b>, desde seu planejamento até sua construção e preservação. Somos gratos a todos os envolvidos nessa parceria. </p>');
	}
</script>